package example

import io.gatling.core.Predef._
import io.gatling.core.structure.{ChainBuilder, ScenarioBuilder}
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import java.io.{BufferedWriter, FileOutputStream, OutputStreamWriter}
import java.time.LocalDate
import java.time.temporal.ChronoUnit.DAYS
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.util.Random

abstract class LoadScenarioBuilder extends Simulation {

  private val r: Random.type = {
    scala.util.Random
  }
  val testData = s"test${Random.alphanumeric take 15 mkString}"
  private val testInt: Int = r.nextInt(100)
  val testDataEdited = s"testEdited${Random.alphanumeric take 15 mkString}"
  private val uuidBatch: String = java.util.UUID.randomUUID.toString
  private val from: LocalDate = LocalDate.of(1970, 1, 1)
  private val to: LocalDate = LocalDate.of(2015, 1, 1)
  private val HOST = "https://workshop1.demonaxiom.com"

  private def generateRandomFile(): Unit = {
    val file = "src/test/resources/data.txt"
    val writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))
    for (_ <- 0 until 100) {

      writer.write(s"test${Random.alphanumeric take 150 mkString}" + "\n")

    }

    writer.close()
  }

  def random(from: LocalDate, to: LocalDate): LocalDate = {
    val diff: Long = DAYS.between(from, to)
    val random = new Random(System.nanoTime)
    from.plusDays(random.nextInt(diff.toInt))
  }

  def createHttpProtocolBuilder(): HttpProtocolBuilder = {
    val httpProtocolBuilder: HttpProtocolBuilder = http
      .contentTypeHeader("application/json")
      .acceptHeader("application/json")
    httpProtocolBuilder
  }

  def createScenarioBuilder(): ScenarioBuilder = {
    val loginStep: ChainBuilder = exec(
      http("Zalogowanie")
        .post(HOST + "/connect/token")
        .formParam("grant_type", "password")
        .asFormUrlEncoded
        .formParam("username", "admin-llcd")
        .asFormUrlEncoded
        .formParam("password", "!Q2w3e4r")
        .asFormUrlEncoded
        .formParam("scope", "internal-api")
        .asFormUrlEncoded
        .formParam("client_secret", "secret")
        .asFormUrlEncoded
        .formParam("client_id", "mvc")
        .asFormUrlEncoded
        .check(status.is(200))
        .check(jsonPath("$.access_token").saveAs("access_token"))).exitHereIfFailed

    val createAttachmentsForControlDependencies: ChainBuilder = exec(
      http("Dodania załącznika do formularza dla kontrolek zależnych")
        .post(HOST + "/back/attachments-module/Attachment/UploadAttachment?recordId=${record_id}&formDefinitionId=7&isToOcr=false&attachmentCategoryId=8")
        .header("authorization", "Bearer ${access_token}")
        .header("content-type", "multipart/form-data; boundary=----WebKitFormBoundaryjjtiImBNEMaK1Dnd")
        .formUpload("file", "data.txt")
        .check(jsonPath("$.isValid").is("true"))
        .check(jsonPath("$.validationResult.errors").is("[]"))
        .check(status.is(200)))

    val filterControlDependenciesByCreatedAtToday: ChainBuilder = exec(
      http("Filtrowanie po dacie utworzenia dla kontrolek zależnych")
        .post(HOST + "/back/Grid/GetGridPartialContent?gridId=6&filterId=0&sampleData=false")
        .header("authorization", "Bearer ${access_token}")
        .header("content-type", "multipart/form-data; boundary=----WebKitFormBoundaryjjtiImBNEMaK1Dnd")
        .body(StringBody(
          "{\"skip\":0,\"take\":50,\"filter\":{\"logic\":\"and\",\"filters\":[{\"logic\":\"and\",\"filters\":[{\"field\":\"CreateDate\",\"operator\":\"gte\",\"value\":\"2022-03-08T00:00:00.000Z\",\"compositeDescriptor\":false},{\"field\":\"CreateDate\",\"operator\":\"lte\",\"value\":\"2022-03-08T23:59:59.000Z\",\"compositeDescriptor\":false}],\"compositeDescriptor\":true}],\"compositeDescriptor\":true},\"group\":[],\"sort\":[],\"localStorageParams\":[]}"))
        .asJson
        .check(jsonPath("$.isValid").is("true"))
        .check(jsonPath("$.validationResult.errors").is("[]"))
        .check(status.is(200)))

    val createAttachmentsForDocumentOCR: ChainBuilder = exec(
      http("Dodania załącznika do formularza dla OCR i MailMonitor")
        .post(HOST + "/back/attachments-module/Attachment/UploadAttachment?recordId=${record_id_ocr}&formDefinitionId=15&isToOcr=false&attachmentCategoryId=11")
        .header("authorization", "Bearer ${access_token}")
        .header("content-type", "multipart/form-data; boundary=----WebKitFormBoundaryhWIxOLAj5ykQqbRl")
        .formUpload("file", "data.txt")
        .check(jsonPath("$.isValid").is("true"))
        .check(jsonPath("$.validationResult.errors").is("[]"))
        .check(status.is(200)))

    val createDocumentControlDependencies: ChainBuilder = exec(http("Utowrzenie dokumentu dla kontrolek zależnych")
      .post(
        HOST + "/back/Action/RunSaveFormSystemAction?recordId=0&rowVersion=0&documentId=7&formId=7&batchId=" + uuidBatch)
      .header("authorization", "Bearer ${access_token}")
      .body(StringBody(
        "\"{\\\"Transition\\\":null,\\\"Status\\\":null,\\\"Type1\\\":null,\\\"Type2\\\":null,\\\"Type3\\\":null,\\\"Id\\\":null,\\\"Code\\\":null,\\\"Organization\\\":null,\\\"RowVersion\\\":null,\\\"UserId\\\":null,\\\"CreateDate\\\":null,\\\"UpdatedBy\\\":null,\\\"UpdatedDate\\\":null,\\\"ACLId\\\":null,\\\"Blocked\\\":null,\\\"BlockUser\\\":null,\\\"LastEntryDate\\\":null,\\\"ArchiveStatus\\\":null,\\\"IsAnonimized\\\":null,\\\"PoleSlownik\\\":null,\\\"PoleZalezne1\\\":null,\\\"PoleZalezne2\\\":0,\\\"PoleListaWyboru\\\":null,\\\"PoleZalezne3\\\":0,\\\"Miasta\\\":null,\\\"Wojewodztwa\\\":null,\\\"PoleZalezne4\\\":null,\\\"PoleZalezne5\\\":null,\\\"Slownik2\\\":1,\\\"Telefon\\\":\\\" " + testInt + " \\\",\\\"RecipientType\\\":null,\\\"Pole5\\\":null,\\\"Pole6\\\":null,\\\"DataZatrudnienia\\\":\\\"" + random(
          from,
          to) + "T00:00:00\\\",\\\"NumerBudynku\\\":1,\\\"FirstName\\\":\\\" " + testData + " \\\",\\\"StatusAkceptacja\\\":null,\\\"PoleHTML\\\":null,\\\"OpenedForms\\\":[],\\\"FormCode\\\":\\\"" + testData + "\\\"}\""))
      .asJson
      .check(jsonPath("$.isValid").is("true"))
      .check(jsonPath("$.validationResult.errors").is("[]"))
      .check(status.is(200))
      .check(jsonPath("$.result").saveAs("record_id")))

    val createDocumentOCR: ChainBuilder = exec(http("Utworzenie dokumentu dla OCR i MailMonitor")
      .post(
        HOST + "/back/Action/RunSaveFormSystemAction?recordId=0&rowVersion=0&documentId=9&formId=7&batchId=" + uuidBatch)
      .header("authorization", "Bearer ${access_token}")
      .body(StringBody(
        "\"{\\\"Transition\\\":null,\\\"Status\\\":null,\\\"Type1\\\":null,\\\"Type2\\\":null,\\\"Type3\\\":null,\\\"Id\\\":null,\\\"Code\\\":null,\\\"Organization\\\":null,\\\"RowVersion\\\":null,\\\"UserId\\\":null,\\\"CreateDate\\\":null,\\\"UpdatedBy\\\":null,\\\"UpdatedDate\\\":null,\\\"ACLId\\\":null,\\\"Blocked\\\":null,\\\"BlockUser\\\":null,\\\"LastEntryDate\\\":null,\\\"ArchiveStatus\\\":null,\\\"IsAnonimized\\\":null,\\\"Sprzedawca\\\":\\\"" + testData + "\\\",\\\"Nabywca\\\":\\\"" + testData + "\\\",\\\"Netto\\\":1,\\\"Brutto\\\":2,\\\"TerminPlatnosci\\\":\\\"" + random(
          from,
          to) + "\\\",\\\"Miejscowosc\\\":\\\"" + testData + "\\\",\\\"KodPocztowy\\\":\\\"" + testInt + "\\\",\\\"Ulica\\\":\\\"" + testData + "\\\",\\\"DataWystawienia\\\":\\\"" + random(
          from,
          to) + "\\\",\\\"OpenedForms\\\":[],\\\"FormCode\\\":\\\"FVry-form\\\"}\""))
      .asJson
      .check(jsonPath("$.isValid").is("true"))
      .check(jsonPath("$.validationResult.errors").is("[]"))
      .check(jsonPath("$.result").saveAs("record_id_ocr"))
      .check(status.is(200)))

    val scn: ScenarioBuilder = scenario("Testy wydajnościowe regresywne").repeat(1) {
      exec(loginStep).doIf("${access_token}".nonEmpty) {
        exec(createDocumentControlDependencies)
          .doIf("${record_id}".nonEmpty) {
            generateRandomFile()
            exec(createAttachmentsForControlDependencies).exitHereIfFailed
          }
          .exec(createDocumentOCR)
          .doIf("${record_id}".nonEmpty) {
            generateRandomFile()
            exec(createAttachmentsForDocumentOCR).exitHereIfFailed
          }
      }
    }
    scn
  }

  def doSetUp(): Unit = {

    val theScenarioBuilder: ScenarioBuilder = createScenarioBuilder()
    val theHttpProtocolBuilder: HttpProtocolBuilder = createHttpProtocolBuilder()

    setUp(
      theScenarioBuilder
        .inject(
          10.to {
              10
            }
            .map((i: Int) => constantConcurrentUsers(i) during 1.minute)
        )
        .protocols(theHttpProtocolBuilder)
    )
  }
}
